package com.example.servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletSample", value = "/ServletSample")
public class ServletSample extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean newUser = true;

        Cookie[] cookies = request.getCookies();


        if (cookies != null){
            for (Cookie c:
                 cookies) {
                if (c.getName().equals("visited") && c.getValue().equals("yes")){
                    newUser = false;
                    break;
                }
            }
        }

        String message = null;

        if (newUser){
            Cookie visitedCookie = new Cookie("visited", "yes");
            response.addCookie(visitedCookie);
            message = "Thanks for visiting";
        }else {
            message = "Thanks for visiting again";
        }

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("message: " + message);
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}