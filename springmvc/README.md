## springmvc

Used for testing web applications source/sinks in springmvc applications.

### Adding tests

Simply create a new package or find the applicable package under `src/main/java/com/gitlab/<type>` to create the new class.
Create the class that demonstrates the vulnerability. 


### Running

Run:
```
mvn package
docker build -t springmvc . && docker run --rm -p 8080:8080 --name springmvc springmvc
# Once started, hit the endpoint, all servlets are under the "/spring-mvc-example" application namespace.
curl -vvv "http://localhost:8080/spring-mvc-example/TestInject"
```